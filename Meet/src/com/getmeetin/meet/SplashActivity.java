/***
 * Copyright (c) GetMeetin 2014
 * 
 * Change log: Change Build number Description Changed by Bug ID 1 Version 1-
 ***/

package com.getmeetin.meet;

import java.io.FileNotFoundException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.SyncStateContract.Constants;
import android.widget.LinearLayout;

import com.getmeetin.utilities.MeetAlertDialog;
import com.getmeetin.utilities.MeetLogger;
import com.getmeetin.utilities.UncaughtExceptionHandler;
import com.getmeetin.utilities.Utility;

public class SplashActivity extends Activity {

	protected boolean _active = true;
	protected int _splashTime = 2000;
	/**
	 * Tag for a class logging
	 */
	private static final String TAG = SplashActivity.class.getSimpleName();

	class ActivitiesBroadcastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			finish();
		}
	}

	private final Handler splashHandler = new Handler() {
		public void handleMessage(Message msg) {
			AlertDialog alertDialog = new MeetAlertDialog(SplashActivity.this)
					.create();
			alertDialog.setCancelable(false);

			// Setting OK Button
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			});
			// Showing Alert Message
			alertDialog.show();
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler(
				getApplicationContext(), TAG));
		setContentView(R.layout.activity_splash);
		MeetLogger.appendInfoLog("Starting SplashActivity...", TAG);

		LinearLayout splashLayout = (LinearLayout) findViewById(R.id.splash_layout);
		if (splashLayout != null) {
			Utility.unStretchBg(R.id.splash_layout, R.drawable.splash, this);
		}
		Thread splashTread = new Thread() {
			@Override
			public void run() {
			}
		};
		splashTread.start();

	}

	protected void onDestroy() {
		super.onDestroy();

	}
}
