/***
 * Copyright (c) GetMeetin 2014
 * 
 * Change log: Change Build number Description Changed by Bug ID 1 Version 1-
 ***/

package com.getmeetin.meet;

import android.app.Application;

@SuppressWarnings("unused")
public class MeetApplication extends Application {

	/**
	 * Tag for a class logging
	 */
	private static final String TAG = MeetApplication.class.getSimpleName();

}
