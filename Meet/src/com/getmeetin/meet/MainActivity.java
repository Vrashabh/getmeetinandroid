/***
 * Copyright (c) GetMeetin 2014
 * 
 * Change log: Change Build number Description Changed by Bug ID 1 Version 1-
 ***/

package com.getmeetin.meet;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static final int REQUEST_ENABLE_BLUETOOTH = 1;
	TextView name = null;
	TextView designation = null;
	TextView address = null;
	TextView phone = null;
	TextView email = null;
	ImageView mImg = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_card_layout);

		findControls();

		setValues();
	}

	private void findControls() {
		name = (TextView) findViewById(R.id.name);
		designation = (TextView) findViewById(R.id.designation);
		address = (TextView) findViewById(R.id.address);
		phone = (TextView) findViewById(R.id.phone);
		email = (TextView) findViewById(R.id.email);
		mImg = (ImageView) findViewById(R.id.imageView2);

		ImageButton bluetoothButton = (ImageButton) this
				.findViewById(R.id.blueetooth);
		bluetoothButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String mCardPath = generateMeetCardFile();

				if (mCardPath != null) {
					Intent intent = new Intent();
					intent.setAction(Intent.ACTION_SEND);
					intent.setType("text/x-vcard");
					File mCardFile = new File(mCardPath);
					intent.putExtra(Intent.EXTRA_STREAM,
							Uri.fromFile(mCardFile));
					startActivity(intent);
				}
			}

			private String generateMeetCardFile() {
				try {
					File root = new File(Environment
							.getExternalStorageDirectory(), "Meet");
					if (!root.exists()) {
						root.mkdirs();
					}

					File meetCard = new File(root, "card.json");
					FileWriter writer = new FileWriter(meetCard);
					// Need to build a vCard type .meet file for exchange with
					// another user
					// writer.append("Hello");
					writer.flush();
					writer.close();

					return meetCard.getAbsolutePath();
					// Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
				} catch (Exception ex) {
					ex.printStackTrace();
					return null;
				}
			}
		});
	}

	private void setValues() {
		// Need to pass appropriate parameters for displaying different user
		// values
		setName();
		setDesignation();
		setAddress();
		setPhone();
		setEmail();
		setUserImage();

		getActionBar().hide();
	}

	private void setName() {
		name.setText("Vishwanath Savakar");
	}

	private void setDesignation() {
		designation.setText("Software Engineer at Flipkart");
	}

	private void setAddress() {
		String addr = "Flipkart.com\n" + "Ashford Park View, #9\n"
				+ "7th Main 80ft Main Rd\n" + "Koramangala 1A Block\n"
				+ "Bangalore - 560 034.";
		address.setText(addr);
	}

	private void setPhone() {
		phone.setText("Phone: 989-898-9898");
		Linkify.addLinks(phone, Linkify.PHONE_NUMBERS);
	}

	private void setEmail() {
		email.setText("e-mail: getmeetin@gmail.com");
		Linkify.addLinks(email, Linkify.EMAIL_ADDRESSES);
	}

	private void setUserImage() {
		try {
			// Bitmap bMap =
			// BitmapFactory.decodeFile("tt.jpg");//"/storage/sdcard0/tt.jpg"
			AssetManager assetManager = getApplicationContext().getAssets();
			InputStream istr = assetManager.open("tt.jpg");
			Bitmap bMap = BitmapFactory.decodeStream(istr);

			Bitmap resized = Bitmap.createScaledBitmap(bMap, 200, 200, true);
			Bitmap conv_bm = getRoundedRectBitmap(resized, 100);

			mImg.setImageBitmap(conv_bm);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public static Bitmap getRoundedRectBitmap(Bitmap bitmap, int pixels) {
		Bitmap result = null;
		try {
			result = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
			Canvas canvas = new Canvas(result);

			int color = 0xff424242;
			Paint paint = new Paint();
			Rect rect = new Rect(0, 0, 200, 200);

			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawCircle(100, 100, 100, paint);
			paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
			canvas.drawBitmap(bitmap, rect, rect, paint);

		} catch (NullPointerException e) {
		} catch (OutOfMemoryError o) {
		}

		return result;
	}
}