/***
Copyright (c) Pengala Learning 2011

Change log:
Change Build number     Description     						   Changed by         Bug ID
1						Version 1-Constants used across the app    Vrashabh			---
 ***/
package com.getmeetin.appconstants;

import java.util.ArrayList;

import com.getmeetin.meet.MeetApplication;

@SuppressWarnings("unused")
public class Constants extends MeetApplication {
	/**
	 * Tag for a class logging
	 */
	private static final String TAG = Constants.class.getSimpleName();
	// Mount paths for multiple variants of sdcards
	public static ArrayList<String> mMounts;
	public static ArrayList<String> mLabels;
	public static String sdCardPath = "";

}
