package com.getmeetin.utilities;

import java.io.*;

import android.content.*;
import android.os.Process;

public class UncaughtExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {
	String Tag;
	Context myContext;
	public UncaughtExceptionHandler(Context context, String TAG) {
        Tag = TAG;
        myContext = context;
    }  

    public void uncaughtException(Thread thread, Throwable exception) {
        StringWriter sw = new StringWriter();
        exception.printStackTrace(new PrintWriter(sw));
        MeetLogger.appendErrorLog(sw.toString(), Tag);
        //Intent intent = new Intent(myContext, ForceClose.class);
        //intent.putExtra(BugReportActivity.STACKTRACE, sw.toString());
       // myContext.startActivity(intent);
        Process.killProcess(Process.myPid());
        System.exit(10);
    }
}