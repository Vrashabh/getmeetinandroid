package com.getmeetin.utilities;

import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

/**
 * overkill - change text colour on click - will be removed after demo
 * 
 * @author peng-lp-10
 * 
 */
public class CustomTouchClass implements View.OnTouchListener {
	public boolean onTouch(View view, MotionEvent motionEvent) {
		switch (motionEvent.getAction()) {
		case MotionEvent.ACTION_DOWN:
			((TextView) view).setBackgroundColor(0xFFFFA500); 
			// white
			break;
		case MotionEvent.ACTION_CANCEL:
		case MotionEvent.ACTION_UP:
			((TextView) view).setTextColor(0xFF000000); // black
			break;
		}
		return false;
	}
}