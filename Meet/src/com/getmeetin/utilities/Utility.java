/***
 * Copyright (c) GetMeetin 2014
 * 
 * Change log: Change Build number Description Changed by Bug ID 1 Version 1-
 ***/

package com.getmeetin.utilities;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.LinearLayout;

import com.getmeetin.appconstants.Constants;

public class Utility {
	/**
	 * Tag for a class logging
	 */
	private static final String TAG = Utility.class.getSimpleName();

	public static String startTag = "<";
	public static String endTag = ">";
	public static String startEmpty = "</";
	public static String end = "/>";

	public static Boolean detectSDCard(String strAppName) {

		StorageOptions.determineStorageOptions();
		int numberOfSdCards = Constants.mMounts.size();
		if (numberOfSdCards == 1) {
			Constants.sdCardPath = "/mnt/sdcard";

		} else {
			for (String item : Constants.mMounts) {
				if (!item.equalsIgnoreCase("/mnt/sdcard")) {
					Constants.sdCardPath = item.toString();
				}
			}
		}

		// Verify the SDCard path
		Boolean retVal = checkForMeet(strAppName);

		if (retVal == false) {
			Constants.sdCardPath = "/mnt/extsd";
		}

		return checkForMeet(strAppName);

	}

	public static Boolean checkForMeet(String strAppName) {
		Boolean retVal = false;
		try {

			File dir = new File(Constants.sdCardPath + "/" + strAppName + "/");

			if (dir.exists() && dir.isDirectory()) {
				retVal = true;
			}

		} catch (Exception ex) {

			MeetLogger.appendErrorLog(ex.toString(), TAG);
			retVal = false;
		}
		return retVal;
	}

	/**
	 * Method to parse xml to doc
	 * 
	 * @param xml
	 * @return
	 */
	public static Document XMLfromString(String xml) {

		Document doc = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {

			DocumentBuilder db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			doc = db.parse(is);

		} catch (ParserConfigurationException e) {
			MeetLogger.appendErrorLog(e.getMessage(), TAG);
			System.out.println("XML parse error: " + e.getMessage());
			return null;
		} catch (SAXException e) {
			MeetLogger.appendErrorLog(e.getMessage(), TAG);
			System.out.println("Wrong XML file structure: " + e.getMessage());
			return null;
		} catch (IOException e) {
			MeetLogger.appendErrorLog(e.getMessage(), TAG);
			System.out.println("I/O exception: " + e.getMessage());
			return null;
		}

		return doc;

	}

	public static void unStretchBg(int layout, int intBackground,
			Activity activity) {

		BitmapFactory.Options myOptions = new BitmapFactory.Options();
		myOptions.inDither = true;
		myOptions.inScaled = false;
		myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
		myOptions.inDither = false;
		myOptions.inPurgeable = true;
		Bitmap preparedBitmap = BitmapFactory.decodeResource(activity
				.getApplication().getResources(), intBackground, myOptions);
		Drawable background = new BitmapDrawable(preparedBitmap);
		((LinearLayout) activity.findViewById(layout))
				.setBackgroundDrawable(background);
	}

	/***
	 * Check if the tablet is connected to the network
	 * 
	 * @return
	 */
	@SuppressWarnings("static-access")
	public static boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public static boolean fileExists(String filePath) {
		File f = new File(filePath);
		if (f.exists()) {
			return true;
		} else {
			return false;
		}
	}

}
